import { Component, OnInit } from '@angular/core';
import { DestinoViaje} from './../models/destino-viaje.model'; //definir para poder usar la clase destino viaje o el componente .. sube un directorio 

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  constructor() {
    this.destinos = [];
   }

  ngOnInit(): void {
  }
    //funcion vinculada a evento click devuelve boleano true o false se setea false para que no recarge pagina

  guardar(nombre:string, url:string): boolean
  {
    this.destinos.push(new DestinoViaje(nombre, url)); //push agrega nuevos objetos al array destinos
    console.log(this.destinos);
    /*console.log(nombre);
    console.log(url);*/
    return false; 

  } 

  elegido(d: DestinoViaje){ //se tiene un destino d del tipo destinoviaje 
    this.destinos.forEach(function (x) {x.setSelected(false);});
    d.setSelected(true);
  }

}
