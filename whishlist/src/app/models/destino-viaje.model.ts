export class DestinoViaje {
  private selected: boolean = false; //se declara variable verifica si esta selecionada
  public servicios: string[]; //agrega string para los servicios del hotel.
  constructor(public nombre: string, public u: string) { 
    this.servicios = ['pileta', 'desayuno']; //se inicializa los servicios. se debe poner en la vista destino-viaje
  }
  //verifica si esta seleccionada
  isSelected(): boolean {
    return this.selected;
  }
  //funcion que  setea como seleccionada
  setSelected(s: boolean) {
    this.selected = s;
  }

}