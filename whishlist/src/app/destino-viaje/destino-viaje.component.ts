import { createInput } from '@angular/compiler/src/core';
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje} from './../models/destino-viaje.model'; //definir para poder usar la clase destino viaje o el componente

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input()
  destino!: DestinoViaje;
  @Input('idx') //sirve para cambiar de nombre a la variable position 
  position!: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output()  //
  clicked!: EventEmitter<DestinoViaje>;  //emitidor de eventos por tipado se coloca DestinoViaje

  constructor() { 
    this.clicked = new EventEmitter();
  }  
// De esta forma inicializa un array vació de Colores, y la variable ya tiene el tipo (Color[]) en su declaración
  
  
  ngOnInit(){
  }
  ir() {
    this.clicked.emit(this.destino); //se define un atrivbuto clikek y emite un evento 
    return false;
  }

}
